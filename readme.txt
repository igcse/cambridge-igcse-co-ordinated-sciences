**ABOUT CONTENT**

Content of the pack should be copied into "clp-uat-repo/Content/secondary/igcse/cambridge/Cambridge_IGCSE_Co-ordinated_Sciences_l" in S3.

Amazon S3 is configured with versioning, hence no more directories should be created to hold updated versions in S3.

Always replace the updated files, versioning allows one to revert to an earlier version.

Use the MathJax hosting available within Connect which is at "https://connect.connect-uat.co.uk/repo1/MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"

MathJax URL should be on single JS file or any configuration file, as best practice to avoid uncertainty when different vendors deal with the content



**ABOUT THE SCORM PACKAGE**

1. Any update made to the manifest or files should be clearly mentioned when the commit is made

2. When an updated pack is loaded to UAT Connect, any previous version available should be thrashed, unless has a definite reason for holding it - But once things are clear this must be dropped.

3. Always use "**https://connect.connect-uat.co.uk/repo1/**Content/.....", as this will avoid the cross domain issue that might otherwise cause.

4. Always use any absolute URSLs with "https", as Connect is secured and would otherwise throw exception

5. Use type element value as "imsqti_xmlv1p2", for QTI XMLs

6. If the QTI is a homework quiz, tagged to a leaf node, available under standard content view, then <resource/> must have the reference of QTI xml and the next immediate <file/> also should have the same reference.

7. In the above case, all dependent files, (images, media files etc.) should be refereed in the  <file/> attribute

8. When the QTI is a ready made test, which should be seen under "Test" tile in connect, should be tagged to a topic

9. Tt should be refereed with the last directory in which the XML is in (e.g. href="Assessments/Biology/"), and the dependent files should not be referred in the <file/> attribute.

10. Every QTI question must have skills and also must have the "remtopicreferenceid" (item identifier of the topic to which the question is related to - The topic related can be sen as "topic reference" in the source document provided by HC editorial). Failing the topic reference in QTI would result in not generating diagnostic report in connect

**BOOK VIEW**

11. Book view link (Front of class view) should have the type value as "bookview" and this specific resource should not be tagged to any <item/> in the pack

12. The bookview with query string should be referred as (this is an example ) "https://connect.connect-uat.co.uk/repo1/Content/UAT/Infuze/BookView/index.html?path=../../JI/Leckie/AQA-KS3-Science-SB1-06-Apr-17-V1/Mapping/2017-05-05_11-06/bookview/book.js"

13. The bookview without query string should be referred as (this is an example) "https://connect.collins.co.uk/repo1/Content/Live/JI/Leckie/AQA%20GCSE_9-1_Chemistry_Combined_Student_Book_New/Contents/wrapper/index.html"

14. Print test area should have the metadata "<metadata<type>printtest</type></metadata> to the parent <item/>